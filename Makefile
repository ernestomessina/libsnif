###############################
# Ernesto Messina
# 2017-03-15
###############################

GXX = g++
FLAGS = -std=c++14 -Wall -fPIC
LIBS = -lpcap
DEBUG = -g
OUTPUTDIR = lib
APPNAME = libsnif.so
# Reads the working directory name
VERSION = $(notdir $(shell pwd))
SOURCESDIR = src/sources


all: create_dir pcap_man.o pcap_excep.o dev_man.o
	$(GXX) $(FLAGS) -shared $(OUTPUTDIR)/*.o -o $(OUTPUTDIR)/$(APPNAME) $(LIBS)
	strip -s $(OUTPUTDIR)/*
	
all-debug: create_dir pcap_man.o pcap_excep.o dev_man.o
	$(GXX) $(FLAGS) -shared $(OUTPUTDIR)/*.o -o $(OUTPUTDIR)/$(APPNAME) $(LIBS)
	

debug: FLAGS += $(DEBUG)
debug: all-debug
	

strip_symbols:
	strip -s $(OUTPUTDIR)/*

pcap_man.o: $(SOURCESDIR)/pcap_man.cpp
	$(GXX) $(FLAGS) -c $(SOURCESDIR)/pcap_man.cpp -o $(OUTPUTDIR)/pcap_man.o

pcap_excep.o: $(SOURCESDIR)/pcap_excep.cpp
	$(GXX) $(FLAGS) -c $(SOURCESDIR)/pcap_excep.cpp -o $(OUTPUTDIR)/pcap_excep.o

dev_man.o: $(SOURCESDIR)/dev_man.cpp
	$(GXX) $(FLAGS) -c $(SOURCESDIR)/dev_man.cpp -o $(OUTPUTDIR)/dev_man.o

create_dir:
	mkdir -p $(OUTPUTDIR)

dist: all remove_object_files strip_symbols create_pkg clean

create_pkg:
	tar cfJ $(APPNAME)-$(VERSION).tar.xz $(OUTPUTDIR)

remove_object_files:
	rm -f $(OUTPUTDIR)/*.o

clean:
	rm -rf $(OUTPUTDIR) *~

help:
	@echo "The following are some of the valid targets:"
	@echo "... all (the default if no target is provided)"
	@echo "... debug"
	@echo "... remove_object_files"
	@echo "... clean"
	@echo "... dist (creates a distribution package)"
