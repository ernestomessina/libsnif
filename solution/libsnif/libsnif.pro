TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lpcap

SOURCES += \
    ../../src/sources/pcap_excep.cpp \
    ../../src/sources/pcap_man.cpp \
    ../../src/sources/dev_man.cpp

HEADERS += \
    ../../src/headers/pcap_excep.h \
    ../../src/headers/pcap_man.h \
    ../../src/headers/dev_man.h
