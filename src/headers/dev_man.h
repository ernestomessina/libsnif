/***
 * Class resposible for caputuring interface managing.
 *
 * @Date: 2017-04-05
 * @Author: Ernesto Messina
**/
#ifndef DEV_MAN_H
#define DEV_MAN_H

#include <memory>
#include <vector>
#include <string>
#include <pcap.h>

class dev_man
{
public:
    dev_man();

    /**
     * @brief Return the current device.
     * @return A pointer to the current device.
     */
    const pcap_if_t* get_current_dev() const noexcept;

    /**
     * @brief Return a constant reference to a vector the devices.
     * @return A reference to the vector devices.
     */
    const std::vector<pcap_if_t>& get_list_dev() noexcept;

    /**
     * @brief Return the default device name.
     * @return A const string reference to the default device name.
     */
    const std::string& get_default_dev_name() const noexcept;

    /**
     * @brief Return the last error.
     * @return A const string reference to the last error string.
     */
    const std::string& get_error() const noexcept;

    /**
     * @brief Set a device as current.
     * @param name Device name.
     */
    void set_dev(const std::string& name) noexcept;

private:
    int generate_list_dev();
    auto load_default_dev() -> const std::string&;

private:
    std::unique_ptr<pcap_if_t> curr_dev;
    std::vector<pcap_if_t> dev_list;
    std::string default_dev_name;
    std::string curr_dev_name;
    std::string errbuf;
};

#endif // DEV_MAN_H
