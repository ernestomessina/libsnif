/***
 * Network traffic sniffer.
 *
 * @Name: libsnif
 * @Date: 2017-03-15
 * @Author: Ernesto Messina
*/
#ifndef PCAP_EXCEP_H
#define PCAP_EXCEP_H

#include <exception>


/**
 * @brief Generic exceptions
 */
class pcap_excep : public std::exception
{
public:
    virtual const char* what() const throw();
};

class pcap_excep_setup : public pcap_excep
{
public:
    virtual const char* what() const throw();
};


/**
 * @brief Device related exceptions
 */
class pcap_excep_device : public pcap_excep
{
public:
    virtual const char* what() const throw();
};

class pcap_excep_cannot_open_dev : public pcap_excep_device
{
public:
    virtual const char* what() const throw();
};

class pcap_excep_cannot_open_file : public pcap_excep_device
{
public:
    virtual const char* what() const throw();
};


/**
 * @brief Network related exceptions
 */
class pcap_excep_net : public std::exception
{
public:
    virtual const char* what() const throw();
};

class pcap_excep_lookup_net : public pcap_excep_net
{
public:
    virtual const char* what() const throw();
};

class pcap_excep_ip_len : public pcap_excep_net
{
public:
    virtual const char* what() const throw();
};

class pcap_excep_tcp_len : public pcap_excep_net
{
public:
    virtual const char* what() const throw();
};


/**
 * @brief Filter related exceptions
 */
class pcap_excep_filter : public std::exception
{
public:
    virtual const char* what() const throw();
};

class pcap_excep_compiling_filter : public pcap_excep_filter
{
public:
    virtual const char* what() const throw();
};

class pcap_excep_setting_filter : public pcap_excep_filter
{
public:
    virtual const char* what() const throw();
};


#endif // PCAP_EXCEP_H
