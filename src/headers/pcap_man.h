/***
 * Network traffic sniffer.
 *
 * @Name: libsnif
 * @Date: 2017-03-15
 * @Author: Ernesto Messina
*/
#ifndef PCAP_MAN_H
#define PCAP_MAN_H

#include <pcap.h>
#include <string>
#include <memory>
#include "pcap_excep.h"
#include "dev_man.h"


class pcap_man
{
public:
    /**
     * @brief Constructor.
     * @param expression The filter expression.
     * @param callback The user callback.
     * @param dev The Sniffing device's name.
     */
    pcap_man(const std::string& expression, pcap_handler callback, const std::string& dev = {});
    ~pcap_man();
    pcap_man(const pcap_man& other) = delete;

    /**
     * @brief Start the sniffing.
     * @return Return 0 on suceess.
     */
    int start_capture();

    /**
     * @brief Stop the sniffing.
     */
    void stop_capture();

    /**
     * @brief Capture while by done reading from file.
     * @param filename Path to the file.
     */
    void capture_from_file(const std::string& _filename);

    /**
     * @brief Let you set the sniffing device.
     * @param dev The device's name.
     */
    void set_device(const std::string& dev);

    /**
     * @brief Return capturing device.
     * @return A string wiht the capturing device name.
     */
    std::string get_device() const;

    /**
     * @brief Return the filter expression.
     * @return A string wiht the filtering expression.
     */
    std::string get_expression() const;

    /**
     * @brief Return the last error message.
     * @return An string containing the last error message.
     */
    std::string get_error() const;

    /**
     * @brief Return the last exception message.
     * @return A string containing the last exception message.
     */
    std::string get_excep_msg() const;

    /**
     * @brief Return a constant reference to a vector the devices.
     * @return A reference to the vector devices.
     */
    const std::vector<pcap_if_t>& get_list_dev() noexcept;

private:
    const std::string& dev_lookup(const std::string& dev);
    pcap_t* open_device(bool promiscuous, int time_out);
    int lookup_net();
    int compile_filter();
    int set_filter();

private:
    dev_man device_man;
    pcap_handler user_callback; // User callback.
    std::string filter_exp; // Filter expression.
    std::string error_msg;
    std::string exception_msg;
    std::string device;
    pcap_t *session {}; // Capturing session.
    bpf_u_int32 net_ip {};
    bpf_u_int32 mask {};
    bool capturing_from_file {};
    struct bpf_program compiled_filter_exp;
    static uint pkg_count;
};


#endif // PCAP_MAN_H
