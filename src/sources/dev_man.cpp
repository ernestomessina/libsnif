#include "../headers/dev_man.h"

dev_man::dev_man()
{
    default_dev_name = load_default_dev();
}

const pcap_if_t* dev_man::get_current_dev() const noexcept
{
    return curr_dev.get();
}

const std::vector<pcap_if_t>& dev_man::get_list_dev() noexcept
{
    if (dev_list.empty())
        generate_list_dev();

    return dev_list;
}

int dev_man::generate_list_dev()
{
    pcap_if_t *all {}, *curr {};
    if (pcap_findalldevs(&all, &*errbuf.begin()) == -1)
        return -1;

    // pcap_findalldevs can return 0 (success) without finding any device
    if (!all) return 0;

    for (curr = all; curr != nullptr; curr = curr->next)
        dev_list.push_back(*curr);

    return 0;
}

const std::string& dev_man::get_default_dev_name() const noexcept
{
    return default_dev_name;
}

const std::string& dev_man::get_error() const noexcept
{
    return errbuf;
}

auto dev_man::load_default_dev() -> const std::string&
{
    default_dev_name = pcap_lookupdev(&*errbuf.begin());
    return default_dev_name;
}

void dev_man::set_dev(const std::string& name) noexcept
{
    curr_dev_name = name;
}
