#include "../headers/pcap_excep.h"


// Generic exceptions
const char* pcap_excep::what() const throw()
{
    return "General exception. I've no idea what happened.";
}

const char* pcap_excep_setup::what() const throw()
{
    return "Error with your settings.";
}


// Device related exceptions
const char* pcap_excep_device::what() const throw()
{
    return "Couldn't set device.";
}

const char* pcap_excep_cannot_open_dev::what() const throw()
{
    return "Cannot open device.";
}

const char* pcap_excep_cannot_open_file::what() const throw()
{
    return "Cannot open file.";
}


// Network related exception
const char* pcap_excep_net::what() const throw()
{
    return "Cannot perform network lookup.";
}

const char* pcap_excep_lookup_net::what() const throw()
{
    return "Cannot perform network lookup.";
}

const char* pcap_excep_ip_len::what() const throw()
{
    return "Invalid IP header length. Too small.";
}

const char* pcap_excep_tcp_len::what() const throw()
{
    return "Invalid TCP header length. Too small.";
}


// Filter related exceptions
const char* pcap_excep_filter::what() const throw()
{
    return "Generic filter error.";
}

const char* pcap_excep_compiling_filter::what() const throw()
{
    return "Error compiling filter.";
}

const char* pcap_excep_setting_filter::what() const throw()
{
    return "Error setting filter.";
}
