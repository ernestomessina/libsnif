#include "../headers/pcap_man.h"
#include <iomanip>  // For printing: setw, setfill
#include <cassert>
//#include "../headers/tcpip-eth.h"

/////////////////////////////////
// Static members initialization
uint pcap_man::pkg_count {};


/////////////////////////////////
// pcap_man definitions
pcap_man::pcap_man(const std::string& expression, pcap_handler callback, const std::string& dev/* = {}*/)
: user_callback { callback }
, filter_exp { expression }
, device { dev }
{
    assert(sizeof(int) == 4);
    assert(sizeof(short) == 2);

    if (!callback)
    {
        pcap_excep_setup excep;
        throw excep;
    }

    // Filtering expression allow us to specify what kind of
    // traffic we want to intercept. Check this out for more:
    // http://www.tcpdump.org/manpages/pcap-filter.7.html
    // https://wiki.wireshark.org/CaptureFilters
    //
    // For instance:
    //filter_exp = "port 80";

    device = dev_lookup(device.c_str());
}

pcap_man::~pcap_man()
{
    if (session) pcap_close(session);
}

const std::string& pcap_man::dev_lookup(const std::string& dev)
{
    pcap_excep_device excep;
    char _errbuf[PCAP_ERRBUF_SIZE];
    device = !dev.empty() ? dev : device_man.get_default_dev_name();

    if (_errbuf == NULL)
    {
        error_msg = _errbuf;
        throw excep;
    }

    return device;
}

int pcap_man::start_capture()
{
    if (!capturing_from_file && device.empty()) dev_lookup(nullptr);

    error_msg.clear();
    try
    {
        lookup_net();
        if (!capturing_from_file)
            open_device(true, 1000);
        compile_filter();
        set_filter();

        // Loop on session, waiting for infitite packages (-1) packages, callback, no user parameters
        pcap_loop(session, -1, user_callback, nullptr);
    }
    catch(pcap_excep_net& e)
    {
        exception_msg = e.what();
        throw e;
    }
    catch(pcap_excep_device& e)
    {
        exception_msg = e.what();
        throw e;
    }
    catch(pcap_excep_filter& e)
    {
        exception_msg = e.what();
        throw e;
    }
    catch(pcap_excep& e)
    {
        exception_msg = e.what();
        throw e;
    }

    return 0;
}

void pcap_man::stop_capture()
{
    pcap_close(session);
    session = nullptr;
}



void pcap_man::capture_from_file(const std::string& _filename)
{
    if (session) pcap_close(session);

    char errbuf[PCAP_ERRBUF_SIZE];
    session = pcap_open_offline(_filename.c_str(), errbuf);
    if (!session)
    {
        error_msg = errbuf;
        pcap_excep_cannot_open_file excep;
        throw excep;
    }

    capturing_from_file = true;
}

std::string pcap_man::get_error() const
{
    return error_msg;
}

std::string pcap_man::get_excep_msg() const
{
    return exception_msg;
}

const std::vector<pcap_if_t>& pcap_man::get_list_dev() noexcept
{
    return device_man.get_list_dev();
}

void pcap_man::set_device(const std::string& dev)
{
    device = dev;
}

std::string pcap_man::get_device() const
{
    return device;
}

std::string pcap_man::get_expression() const
{
    return filter_exp;
}

pcap_t* pcap_man::open_device(bool promiscuous, int time_out)
{
    if (session) pcap_close(session);

    pcap_excep_cannot_open_dev excep;
    char _errbuf[PCAP_ERRBUF_SIZE];
    session = pcap_open_live(device.c_str(), BUFSIZ, promiscuous ? 1 : 0, time_out, _errbuf);
    if (!session)
    {
        error_msg = "Couldn't open device: " + device + ": " + std::string(_errbuf);
        throw excep;
    }

    return session;
}

int pcap_man::lookup_net()
{
    char _errbuf[PCAP_ERRBUF_SIZE];

    int res { pcap_lookupnet(device.c_str(), &net_ip, &mask, _errbuf) };
    if (res == -1)
    {
        net_ip = 0;
        mask = 0;
        error_msg = "Can't get netmask for device: " + std::string(_errbuf);
        pcap_excep_lookup_net excep;
        throw excep;
    }

    return res;
}

int pcap_man::compile_filter()
{
    pcap_excep_compiling_filter excep;
    int res { pcap_compile(session, &compiled_filter_exp, filter_exp.c_str(), 0, net_ip) };
    if (res == -1)
    {
        error_msg = "Couldn't parse filter: " + filter_exp + pcap_geterr(session);
        throw excep;
    }

    return res;
}

int pcap_man::set_filter()
{
    int res { pcap_setfilter(session, &compiled_filter_exp) };
    pcap_freecode(&compiled_filter_exp);
    if (res == -1)
    {
        error_msg = "Couldn't install filter: " + filter_exp + pcap_geterr(session);
        pcap_excep_setting_filter excep;
        throw excep;
    }

    return res;
}
